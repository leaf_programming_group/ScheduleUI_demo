import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
import store from './store/index.js'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import {postRequest} from "./utils/api.js";
import {postKeyValueRequest} from "./utils/api.js";
import {putRequest} from "./utils/api.js";
import {deleteRequest} from "./utils/api.js";
import {getRequest} from "./utils/api.js";
import {initMenu} from "./utils/menus.js";

Vue.prototype.postRequest = postRequest;
Vue.prototype.postKeyValueRequest = postKeyValueRequest;
Vue.prototype.putRequest = putRequest;
Vue.prototype.deleteRequest = deleteRequest;
Vue.prototype.getRequest = getRequest;

Vue.config.productionTip = false

Vue.use(ElementUI,{size:'small'});

router.beforeEach((to, from, next) => {
	if (to.meta.title) {
	        document.title = to.meta.title;
	    }
    if (to.path == '/') {
        next();
    }else {
        if (window.sessionStorage.getItem("user")) {
			document.title = to.meta.title;
            initMenu(router, store);
            next();
        }else{
            next('/?redirect='+to.path);
        }
    }
})

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
