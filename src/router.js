import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/login.vue'
import Home from './components/homepage.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login,
            hidden:true,
			meta:{
			    title: '欢迎登录系统',
			},
        }, {
            path: '/home',
            name: 'Home',
            component: Home,
            hidden:true,
            meta:{
                title: '首页',
				requireAuth:true,
            },
            
        }
    ]
})